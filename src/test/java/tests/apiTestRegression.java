package tests;

import com.google.common.base.Functions;
import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.http.ContentType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.json.JSONArray;
import org.testng.annotations.Test;
import pages.DetailsPage;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;



public class apiTestRegression extends TestBase {

    @Test(priority = 1, dataProvider = "dataProvider", description = "get status code")
    @Severity(SeverityLevel.CRITICAL)
    @Description("get status code")

    public void getStatus(String path, String statusCode) {

            startTest("getStatus");
            reportInfo("verify status code");
            given()
                    .when()
                    .get(path)
                    .then()
                    .contentType(ContentType.JSON)
                    .statusCode(Integer.parseInt(statusCode))
                    .log().all();
            endTest("getStatus");


    }

    @Test(priority = 1, dataProvider = "dataProvider", description = "create post")
    @Severity(SeverityLevel.CRITICAL)
    @Description("create post")

    public void createPost(String path, String firstname, String lastname, String totalPrice,
                           String depositPaid, String checkin, String checkout,
                           String additionalNeeds, String statusCode) throws Exception {
        startTest("createPost");
        reportInfo("create post");

        bookings.firstname(firstname);
        bookings.lastname(lastname);
        bookings.totalPrice(totalPrice);
        bookings.depositPaid(depositPaid);
        bookings.bookingdates(checkin, checkout);
        bookings.additionalNeeds(additionalNeeds);

        bookingId =
        given()
                .body(bookings.jsonParameter().toString()).log().all()
                .when()
                .post(path)
                .then()
                .log().all()
                .contentType(ContentType.JSON)
                .statusCode(Integer.parseInt(statusCode))
                .and()
                .extract()
                .path("bookingid")
                .toString();
        endTest("createPost");



    }





}

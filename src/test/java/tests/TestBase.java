package tests;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import endpoints.Bookings;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.parsing.Parser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.HomePage;
import util.DataDrivenHelper;
import util.Log;
//import util.WebDriverHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class TestBase {


    static ExtentTest test;
    private static ExtentReports extent;
    private WebDriver driver;

    private Properties testConfig;
    public String accessToken;

    private String page = Thread.currentThread().getStackTrace()[1].getClassName();
    JsonObject jsonParameter = new JsonObject();
    Bookings bookings = new Bookings();

    public String bookingId;

    public static String randomDate(int startYear, int endYear) {
        String month;
        GregorianCalendar gc = new GregorianCalendar();
        int year = startYear + (int) Math.round(Math.random() * (endYear - startYear));
        gc.set(Calendar.YEAR, year);
        int dayOfYear = 1 + (int) Math.round(Math.random() * (gc.getActualMaximum(Calendar.DAY_OF_YEAR) - 1));
        gc.set(Calendar.DAY_OF_YEAR, dayOfYear);
        if (gc.get(Calendar.MONTH) + 1 < 10) {
            month = "0" + gc.get(Calendar.MONTH) + 1;
        } else {
            month = Integer.toString(gc.get(Calendar.MONTH) + 1);
        }
        String day;
        if (gc.get(Calendar.DAY_OF_MONTH) < 10) {
            day = "0" + gc.get(Calendar.DAY_OF_MONTH);
        } else {
            day = Integer.toString(gc.get(Calendar.DAY_OF_MONTH));
        }
        return day + month + gc.get(Calendar.YEAR);
    }


    @BeforeSuite
    public void beforeSuite() throws IOException {
        testConfig = new Properties();
        testConfig.load(new FileInputStream("TestConfig.properties"));


        // Allure reports directory handling
        try {
            File allureDir = new File(System.getProperty("user.dir") + "/allure-results/");
            FileUtils.cleanDirectory(allureDir);
        } catch (Exception ignored) {
        }

        // Extent reports initialisation
        extent = new ExtentReports(System.getProperty("user.dir") + "/extent-results/ExtentReport.html");
        extent.loadConfig(new File(System.getProperty("user.dir") + "/extent-results/extent-config.xml"));
        extent.addSystemInfo("Host Name", "SWLGWAPPT03 Test Automation Server");
        extent.addSystemInfo("Environment", "Windows 10");
    }

    @BeforeMethod
    public void beforeMethod(Method method) {

        jsonParameter.addProperty("username", testConfig.getProperty("user"));
        jsonParameter.addProperty("password", testConfig.getProperty("password"));

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .setBaseUri(testConfig.getProperty("baseUrl"))
                .build();
        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectResponseTime(lessThanOrEqualTo(5000L))
                .build();
        RestAssured.registerParser("text/plain", Parser.JSON);

        accessToken =

        given()
                .body(jsonParameter.toString())
                .when()
                .post(testConfig.getProperty("authPath"))
                .then()
                .extract()
                .path("token")
                .toString();

        // Extent report details
        test = extent.startTest((this.getClass().getSimpleName() + " :: " + method.getName()), method.getName());
        test.assignAuthor("Keith");
        test.assignCategory("Regression Report");

    }

    @DataProvider
    public Object[][] dataProvider(Method method) {
        DataDrivenHelper ddh = new DataDrivenHelper(testConfig.getProperty("mastertestdatafile"));
        return ddh.getTestCaseDataSets(testConfig.getProperty("testdatasheet"), method.getName());
    }

    @AfterMethod
    public void afterMethod(ITestResult result) {
        reportInfo("Test completed");
        if (ITestResult.FAILURE == result.getStatus()) {
            reportFail("Test FAILED :: " + result.getThrowable());
            Log.error("------------> TEST FAILED <------------");
            System.out.println("Test FAILED");
        } else {
            reportPass("Test PASSED");
            Log.info("------------> TEST PASSED <------------");
            System.out.println("Test PASSED");
        }
        extent.endTest(test);

    }


    @AfterSuite
    public void afterSuite() {
        extent.flush();
        extent.close();
    }

    void sleep(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
            Log.info(" " + page + " -" + " ..Wait :: " + seconds + " seconds");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getAlphaNumeric(int length, String type) {
        Log.info(" " + page + " -" + " ..Random string generated with length: " + length +
                " ..Type of string: " + type);
        switch (type.toLowerCase()) {
            case "lower":
                return RandomStringUtils.random(length, true, false).toLowerCase();
            case "upper":
                return RandomStringUtils.random(length, true, false).toUpperCase();
            case "digits":
                return RandomStringUtils.random(length, false, true);
            default:
                return RandomStringUtils.random(length, true, true);
        }
    }

    String getUniqueName(int length) {
        Log.info(" " + page + " -" + " ..Unique name generated with length: " + length);
        return RandomStringUtils.random(length, true, false).toLowerCase();
    }

    String getUniqueEmail() {
        Log.info(" " + page + " -" + " ..Unique email generated");
        return getUniqueName(6) + "." + getUniqueName(7) + "@integration.test";
    }

    boolean verifyTextContains(String actualText, String expectedText) {
        Log.info(" " + page + " -" + " ..Verify if text: " + actualText + " ..Contains text: " + expectedText + " ..Result is: " + actualText.contains(expectedText));
        return actualText.contains(expectedText);
    }

    boolean verifyTextMatch(String actualText, String expectedText) {
        Log.info(" " + page + " -" + " ..Verify if text: " + actualText + " ..Matches text: " + expectedText + " ..Result is: " + actualText.contains(expectedText));
        return actualText.equals(expectedText);
    }

    void setUrl(String url) {
        Log.info(" " + page + " -" + " ..Set url to: " + url);
        driver.get(url);
    }

    static void reportInfo(String message) {
        test.log(LogStatus.INFO, message);
    }

    protected static void reportError(String message) {
        test.log(LogStatus.ERROR, message);
    }

    private static void reportPass(String message) {
        test.log(LogStatus.PASS, message);
    }

    private static void reportFail(String message) {
        test.log(LogStatus.FAIL, message);
    }

    static void startTest(String name) {
        Log.startTestCase(name);
    }

    static void endTest(String name) {
        Log.endTestCase(name);
    }
}
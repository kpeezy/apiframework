//package tests;
//
//import io.qameta.allure.Description;
//import io.qameta.allure.Severity;
//import io.qameta.allure.SeverityLevel;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//public class RegressionTest extends TestBase {
//
//
//    @Test(priority = 2, dataProvider = "dataProvider", description = "Update Incident")
//    @Severity(SeverityLevel.CRITICAL)
//    @Description("HSSE-1 : Update a Broken Asset Incident")
//
//    public void UpdateAssetBrokenTest(String incidentType, String incidentNumber, String incidentRow,
//                                      String incidentCategory, String incidentSubCategory, String riskCategory,
//                                      String additionalText) {
//
//        startTest("UpdateAssetBrokenTest");
//        reportInfo("Log into the web page");
//        homePage.loginAccount(user, password);
//        reportInfo("Navigate to the home page and select incident type");
//        homePage.completeSearchType(incidentType);
//        reportInfo("Search for a specific incident");
//        searchPage.completeIncidentSearch(incidentNumber, incidentRow);
//        reportInfo("Change dropdown options");
//        detailsPage.dropdownIncidentCategory(incidentCategory);
//        detailsPage.dropdownIncidentSubCategory(incidentSubCategory);
//        detailsPage.dropdownRiskCategory(riskCategory);
//        detailsPage.textAdditionalDetails(additionalText);
//        reportInfo("Save changes");
//        detailsPage.buttonSaveChanges();
//        endTest("UpdateAssetBrokenTest");
//    }
//
//    @Test(priority = 2, dataProvider = "dataProvider", description = "Update Incident")
//    @Severity(SeverityLevel.CRITICAL)
//    @Description("HSSE-2 : Update a Theft or Abuse Incident")
//
//    public void UpdateTheftAbuseTest(String incidentType, String incidentNumber, String incidentRow,
//                                     String incidentCategory, String offenderArrivalMethod,
//                                     String offenderEntryMethod, String abuseReason, String staffMembersHarmed, String additionalText) {
//        startTest("UpdateTheftAbuseTest");
//        reportInfo("Log into the web page");
//        homePage.loginAccount(user, password);
//        reportInfo("Navigate to the home page and select incident type");
//        homePage.completeSearchType(incidentType);
//        reportInfo("Search for a specific incident");
//        searchPage.completeIncidentSearch(incidentNumber, incidentRow);
//        reportInfo("Change dropdown options");
//        detailsPage.dropdownIncidentCategory(incidentCategory);
//        detailsPage.buttonWarningYes();
//        detailsPage.dropdownOffenderArrivalMethod(offenderArrivalMethod);
//        detailsPage.dropdownOffenderEntryMethod(offenderEntryMethod);
//        detailsPage.dropdownAbuseReason(abuseReason);
//        detailsPage.dropdownStaffMemberHarmed(staffMembersHarmed);
//        detailsPage.textAdditionalDetails(additionalText);
//        reportInfo("Save changes");
//        detailsPage.buttonSaveChanges();
//        endTest("UpdateTheftAbuseTest");
//    }
//
//    @Test(priority = 2, dataProvider = "dataProvider", description = "Update Incident")
//    @Severity(SeverityLevel.CRITICAL)
//    @Description("HSSE-3 : Update a Fire Incident")
//
//    public void UpdateFireTest(String incidentType, String incidentNumber, String incidentRow,
//                               String incidentCategory, String location,
//                               String additionalText) {
//
//        startTest("UpdateFireTest");
//        reportInfo("Log into the web page");
//        homePage.loginAccount(user, password);
//        reportInfo("Navigate to the home page and select incident type");
//        homePage.completeSearchType(incidentType);
//        reportInfo("Search for a specific incident");
//        searchPage.completeIncidentSearch(incidentNumber, incidentRow);
//        reportInfo("Change dropdown options");
//        detailsPage.dropdownIncidentCategory(incidentCategory);
//        detailsPage.dropdownLocation(location);
//        detailsPage.textAdditionalDetails(additionalText);
//        reportInfo("Save changes");
//        detailsPage.buttonSaveChanges();
//        endTest("UpdateTheftAbuseTest");
//    }
//
//    @Test(priority = 2, dataProvider = "dataProvider", description = "Update Incident")
//    @Severity(SeverityLevel.CRITICAL)
//    @Description("HSSE-4 : Update a Food Safety Incident")
//
//    public void UpdateFoodSafetyTest(String incidentType, String incidentNumber, String incidentRow,
//                               String incidentCategory, String incidentSubCategory, String riskCategory, String foodType,
//                               String supplier, String additionalText) {
//
//        startTest("UpdateFoodSafetyTest");
//        reportInfo("Log into the web page");
//        homePage.loginAccount(user, password);
//        reportInfo("Navigate to the home page and select incident type");
//        homePage.completeSearchType(incidentType);
//        reportInfo("Search for a specific incident");
//        searchPage.completeIncidentSearch(incidentNumber, incidentRow);
//        reportInfo("Change dropdown options");
//        detailsPage.dropdownIncidentCategory(incidentCategory);
//        detailsPage.dropdownIncidentSubCategory(incidentSubCategory);
//        detailsPage.dropdownRiskCategory(riskCategory);
//        detailsPage.dropdownFoodType(foodType);
//        detailsPage.dropdownSupplier(supplier);
//        detailsPage.textAdditionalDetails(additionalText);
//        reportInfo("Save changes");
//        detailsPage.buttonSaveChanges();
//        endTest("UpdateTheftAbuseTest");
//    }
//
//    @Test(priority = 2, dataProvider = "dataProvider", description = "Update Incident")
//    @Severity(SeverityLevel.CRITICAL)
//    @Description("HSSE-5 : Update a Injury or Illness Incident")
//
//    public void UpdateInjuryIllnessTest(String incidentType, String incidentNumber, String incidentRow,
//                                        String riskCategory, String source, String location, String additionalText) {
//
//        startTest("UpdateInjuryIllnessTest");
//        reportInfo("Log into the web page");
//        homePage.loginAccount(user, password);
//        reportInfo("Navigate to the home page and select incident type");
//        homePage.completeSearchType(incidentType);
//        reportInfo("Search for a specific incident");
//        searchPage.completeIncidentSearch(incidentNumber, incidentRow);
//        reportInfo("Change dropdown options");
//        detailsPage.dropdownRiskCategory(riskCategory);
//        detailsPage.dropdownSource(source);
//        detailsPage.dropdownLocation(location);
//        detailsPage.textAdditionalDetails(additionalText);
//        reportInfo("Save changes");
//        detailsPage.buttonSaveChanges();
//        endTest("UpdateTheftAbuseTest");
//    }
//
//    @Test(priority = 2, dataProvider = "dataProvider", description = "Update Incident")
//    @Severity(SeverityLevel.CRITICAL)
//    @Description("HSSE-5 : Update a Spills or LPG Leak Incident")
//
//    public void UpdateSpillsLPGLeakTest(String incidentType, String incidentNumber, String incidentRow, String incidentCategory,
//                                        String riskCategory, String source, String location, String additionalText) {
//
//        startTest("UpdateSpillsLPGLeakTest");
//        reportInfo("Log into the web page");
//        homePage.loginAccount(user, password);
//        reportInfo("Navigate to the home page and select incident type");
//        homePage.completeSearchType(incidentType);
//        reportInfo("Search for a specific incident");
//        searchPage.completeIncidentSearch(incidentNumber, incidentRow);
//        reportInfo("Change dropdown options");
//        detailsPage.dropdownIncidentCategory(incidentCategory);
//        detailsPage.dropdownRiskCategory(riskCategory);
//        detailsPage.dropdownSource(source);
//        detailsPage.dropdownLocation(location);
//        detailsPage.textAdditionalDetails(additionalText);
//        reportInfo("Save changes");
//        detailsPage.buttonSaveChanges();
//        endTest("UpdateTheftAbuseTest");
//    }
//
//
//
//}
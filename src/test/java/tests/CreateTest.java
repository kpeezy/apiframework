//package tests;
//
//import io.qameta.allure.Description;
//import io.qameta.allure.Severity;
//import io.qameta.allure.SeverityLevel;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//public class CreateTest extends TestBase {
//
//    @Test(priority = 2, dataProvider = "dataProvider", description = "Update Incident")
//    @Severity(SeverityLevel.CRITICAL)
//    @Description("HSSE-2 : Update a Theft or Abuse Incident")
//
//    public void UpdateTheftAbuseTest(String incidentType, String incidentNumber, String incidentRow,
//                                      String incidentCategory, String offenderArrivalMethod,
//                                     String offenderEntryMethod) {
//        startTest("UpdateTheftAbuseTest");
//        reportInfo("Log into the web page");
//        homePage.loginAccount(user, password);
//        reportInfo("Navigate to the home page and select incident type");
//        homePage.completeSearchType(incidentType);
//        reportInfo("Search for a specific incident");
//        searchPage.completeIncidentSearch(incidentNumber, incidentRow);
//        reportInfo("Change dropdown options");
//        detailsPage.dropdownIncidentCategory(incidentCategory);
//        detailsPage.dropdownOffenderArrivalMethod(offenderArrivalMethod);
//        detailsPage.dropdownOffenderEntryMethod(offenderEntryMethod);
//        endTest("UpdateTheftAbuseTest");
//    }
//}
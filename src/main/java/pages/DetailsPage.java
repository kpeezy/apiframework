package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.assertTrue;

public class DetailsPage extends ActionBase {

    private String page = Thread.currentThread().getStackTrace()[1].getClassName();

    public DetailsPage(WebDriver driver) {
        super(driver);
    }

    public void textIncidentDate(String date) {
        deleteAll(page, LocatorType.Id, "IncidentDate");
        type(page, LocatorType.Id, "IncidentDate", date);
    }

    public void textIncidentTime(String time) {
        deleteAll(page, LocatorType.Id, "IncidentTime");
        type(page, LocatorType.Id, "IncidentTime", time);
    }

    public void textReporterName(String name) {
        type(page, LocatorType.Id, "ReporterName", name);
    }

    public void textUserName(String name) {
        type(page, LocatorType.Id, "Username", name);
    }

    public void textUserEmail(String email) {
        type(page, LocatorType.Id, "UserEmail", email);
    }

    public void dropdownIncidentCategory(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Incident Category']/select[1]", option);
        }
    }

    public void dropdownIncidentSubCategory(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Incident Subcategory']/select[1]", option);
        }
    }

    public void dropdownRiskCategory(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Risk Category']/select[1]", option);
        }
    }

    public void dropdownPersonAffected(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Person Affected']/select[1]", option);
        }
    }

    public void dropdownSource(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Source']/select[1]", option);
        }
    }

    public void dropdownCause(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Cause']/select[1]", option);
        }
    }

    public void dropdownRiskConsequence(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Risk Consequence']/select[1]", option);
        }
    }

    public void dropdownAbuseReason(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Abuse Reason']/select[1]", option);
        }
    }

    public void dropdownOffenderArrivalMethod(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Offender Arrival Method']/select[1]", option);
        }
    }

    public void dropdownOffenderEntryMethod(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Offender Entry Method']/select[1]", option);
        }
    }

    public void dropdownLocation(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Location']/select[1]", option);
        }
    }

    public void dropdownPotentialConsequence(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Potential Consequence']/select[1]", option);
        }
    }

    public void dropdownActualConsequence(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Actual Consequence']/select[1]", option);
        }
    }

    public void dropdownFoodType(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Food Type']/select[1]", option);
        }
    }

    public void dropdownSupplier(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.XPath, "//div[label='Supplier']/select[1]", option);
        }
    }

    public void textProductName(String product) {
        if (!StringUtils.isBlank(product) || !StringUtils.isEmpty(product)) {
            typeWithTab(page, LocatorType.Id, "ProductName", product, "1");
        }
    }

    public void textProductExpiry(String expiry) {
        if (!StringUtils.isBlank(expiry) || !StringUtils.isEmpty(expiry)) {
            typeWithTab(page, LocatorType.Id, "ProductExpiry", expiry, "1");
        }
    }

    public void textProductBatchNumber(String batch) {
        if (!StringUtils.isBlank(batch) || !StringUtils.isEmpty(batch)) {
            typeWithTab(page, LocatorType.Id, "ProductBatchNumber", batch, "1");
        }
    }

    public void dropdownStaffMemberHarmed(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "StaffMemberHarmed", option);
        }
    }

    public void dropdownWeaponsInvolved(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "WeaponsInvolved", option);
        }
    }

    public void dropdownSafeRoomUsed(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "SafeRoomUsed", option);
        }
    }

    public void dropdownFogCannonUsed(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "FogCannonUsed", option);
        }
    }

    public void textNumberOfOffenders(String number) {
        if (!StringUtils.isBlank(number) || !StringUtils.isEmpty(number)) {
            typeWithTab(page, LocatorType.Id, "NumberOfOffenders", number, "1");
        }
    }

    public void dropdownHSSERisk(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "HsseRisk", option);
        }
    }

    public void dropdownSpillSafetyEvent(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "SpillSafetyEvent", option);
        }
    }

    public void dropdownInjurySafetyEvent(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "InjurySafetyEvent", option);
        }
    }

    public void dropdownFireSafetyEvent(String option) {
        if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
            selectItem(page, LocatorType.Id, "FireSafetyEvent", option);
        }
    }

    public String returnLatitude() {
        return getText(page, LocatorType.Id, "Latitude");
    }

    public String returnLongitude() {
        return getText(page, LocatorType.Id, "Longitude");
    }

    public void textAdditionalDetails(String details) {
        type(page, LocatorType.XPath, "//textarea[@class='form-control']", details);
    }

    public void buttonSaveChanges() {

        click(page, LocatorType.XPath, "//button[@class='btn btn-block btn-success']");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement msgBox = driver.findElement(By.xpath("//div[@class='toast-message']"));
        wait.until(ExpectedConditions.visibilityOf(msgBox));
        String actualMsg = msgBox.getText();
        String expectedMsg = "Incident has been updated.";
        assertTrue(actualMsg.contains(expectedMsg));

    }


    public void buttonWarningYes() {

        try {
            if (driver.findElements(By.xpath("//button[@class='btn btn-primary']")).size() != 0) {
                click(page, LocatorType.XPath, "//button[@class='btn btn-primary']");
            }
        } catch (NullPointerException e) {

        }
    }

    public void buttonWarningNo() {
        click(page, LocatorType.XPath, "//button[@class='btn btn-default']");
    }


    public int converToInt(String string) {
        String val = string;
        int statusCode = Integer.parseInt(val);
        return statusCode;
    }


}
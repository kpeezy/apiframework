package pages;

import org.openqa.selenium.WebDriver;

import java.util.Random;

public class SearchPage extends ActionBase {

    private String page = Thread.currentThread().getStackTrace()[1].getClassName();

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    private void textIncidentNumber(String incident) {
        sleep(page, 1);
        typeWithTab(page, LocatorType.XPath, "//div[1]/div[1]/input[1]", incident, "1");
    }

    private void openIncident(String incidentRow) {
        click(page, LocatorType.XPath, "//body//td[" + incidentRow + "]");
        sleep(page, 1);
    }

    public void tabSearch() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Search')]");
        sleep(page, 1);
    }

    public void tabDetails() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Details')]");
        sleep(page, 1);
    }

    public void tabItemsTaken() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Items Taken')]");
        sleep(page, 1);
    }

    public void completeIncidentSearch(String incidentNumber, String incidentRow) {



        int[] randomNumbers = new int[] { 3, 2 };
        Random r = new Random();
        int randomNumber = r.nextInt(randomNumbers.length)+1;
        textIncidentNumber(incidentNumber+randomNumber);
        openIncident(incidentRow);
        tabDetails();
    }
}
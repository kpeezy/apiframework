package pages;

import org.openqa.selenium.WebDriver;

public class ActionBase extends PageBase {

    public ActionBase(WebDriver driver) {
        super(driver);
    }

    public ActionBase click(String page, LocatorType locatorType, String locator) {
        elementClick(page, locatorType, locator);
        return new ActionBase(driver);
    }

    public ActionBase type(String page, LocatorType locatorType, String locator, String text) {
        sendKeys(page, locatorType, locator, text);
        return new ActionBase(driver);
    }

    public ActionBase typeWithTab(String page, LocatorType locatorType, String locator, String text, String tabs) {
        sendKeys(page, locatorType, locator, text);
        elementTabKey(page, locatorType, locator, tabs);
        return new ActionBase(driver);
    }

    public ActionBase typeWithEnter(String page, LocatorType locatorType, String locator, String text) {
        sendKeys(page, locatorType, locator, text);
        elementEnter(page, locatorType, locator);
        return new ActionBase(driver);
    }

    public ActionBase typeWithBackSpace(String page, LocatorType locatorType, String locator, String text, String backSpaces) {
        sendKeys(page, locatorType, locator, text);
        elementBackSpace(page, locatorType, locator, backSpaces);
        return new ActionBase(driver);
    }

    public ActionBase deleteAll(String page, LocatorType locatorType, String locator) {
        elementDeleteAll(page, locatorType, locator);
        return new ActionBase(driver);
    }

    public ActionBase downArrow(String page, LocatorType locatorType, String locator, String repeats) {
        elementDownArrow(page, locatorType, locator, repeats);
        return new ActionBase(driver);
    }

    public ActionBase upArrow(String page, LocatorType locatorType, String locator, String repeats) {
        elementUpArrow(page, locatorType, locator, repeats);
        return new ActionBase(driver);
    }

    public ActionBase leftArrow(String page, LocatorType locatorType, String locator, String repeats) {
        elementLeftArrow(page, locatorType, locator, repeats);
        return new ActionBase(driver);
    }

    public ActionBase rightArrow(String page, LocatorType locatorType, String locator, String repeats) {
        elementRightArrow(page, locatorType, locator, repeats);
        return new ActionBase(driver);
    }

    public ActionBase scrollPage(String page, String direction) {
        webScroll(page, direction);
        return new ActionBase(driver);
    }

    public ActionBase clear(String page, LocatorType locatorType, String locator) {
        clearField(page, locatorType, locator);
        return new ActionBase(driver);
    }

    public ActionBase selectItem(String page, LocatorType locatorType, String locator, String item) {
        elementSelect(page, locatorType, locator, item);
        return new ActionBase(driver);
    }

    public ActionBase check(String page, LocatorType locatorType, String locator, String option) {
        elementCheck(page, locatorType, locator, option);
        return new ActionBase(driver);
    }

    public ActionBase frame(String page, String frame) {
        switchFrame(page, frame);
        return new ActionBase(driver);
    }

    public ActionBase window(String page) {
        switchWindow(page);
        return new ActionBase(driver);
    }

    public ActionBase alert(String page, String response) {
        alertResponse(page, response);
        return new ActionBase(driver);
    }

    public ActionBase dragDrop(String page, LocatorType fromLocatorType, String fromLocator, LocatorType toLocatorType, String toLocator) {
        dragAndDrop(page, fromLocatorType, fromLocator, toLocatorType, toLocator);
        return new ActionBase(driver);
    }

    public ActionBase mouseHover(String page, LocatorType LocatorType, String Locator) {
        mouseHovering(page, LocatorType, Locator);
        return new ActionBase(driver);
    }
}
package pages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.openqa.selenium.WebDriver;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class HomePage extends ActionBase {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    private String page = Thread.currentThread().getStackTrace()[1].getClassName();
    JsonObject jsonParameter = new JsonObject();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public void waitHomePage() {
        sleep(page, 1);
        click(page, LocatorType.XPath, "//a[@class='navbar-brand pull-left'][contains(text(),'Reported Incidents Editor')]");
    }

    private void buttonAbuseTheft() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Abuse / Theft')]");
    }

    private void buttonAssetBroken() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Asset Broken')]");
    }

    private void buttonFire() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Fire')]");
    }

    private void buttonFoodSafety() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Food Safety')]");
    }

    private void buttonInjuryIllness() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Injury / Illness')]");
    }

    private void buttonSpillsLPG() {
        click(page, LocatorType.XPath, "//a[contains(text(),'Spills / LPG Leak')]");
    }


    public void completeSearchType(String type) {
        waitHomePage();
        if (!StringUtils.isEmpty(type) || !StringUtils.isBlank(type)) {
            if (type.toLowerCase().contains("abuse") || type.toLowerCase().contains("theft")) {
                buttonAbuseTheft();
            } else if (type.toLowerCase().contains("asset") || type.toLowerCase().contains("broken")) {
                buttonAssetBroken();
            } else if (type.toLowerCase().contains("fire")) {
                buttonFire();
            } else if (type.toLowerCase().contains("food") || type.toLowerCase().contains("safety")) {
                buttonFoodSafety();
            } else if (type.toLowerCase().contains("injury") || type.toLowerCase().contains("illness")) {
                buttonInjuryIllness();
            } else if (type.toLowerCase().contains("spill") || type.toLowerCase().contains("lpg")) {
                buttonSpillsLPG();
            }
        } else buttonAbuseTheft();
    }

    public void loginAccount(String account, String password) {
        sendKeys(page, LocatorType.Name, "loginfmt", account);
        elementClick(page, LocatorType.XPath, "//input[@type='submit']");
        sendKeys(page, LocatorType.Name, "Password", password);
        elementClick(page, LocatorType.XPath, "//span[@id='submitButton']");
        elementClick(page, LocatorType.XPath, "//input[@type='submit']");
    }

    public void bookingdates(String checkin, String checkout){

        Map<String, String> dates = new HashMap<>();
        dates.put("checkin", checkin);
        dates.put("checkout", checkout);
        jsonParameter.add("bookingdates", gson.toJsonTree(dates));

    }
}
package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import util.Log;

import java.util.List;

public abstract class PageBase {

    protected WebDriver driver;
    private WebDriverWait wait;

    PageBase(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
    }

    protected String getTitle() {
        return driver.getTitle();
    }

    enum LocatorType {Id, Name, Css, Class, Link, XPath}

    private void errorHandler(String method, String message, Exception e) {
        Log.error(message);
        Log.error(" " + method + " -" + " " + e.getMessage());
        if (!StringUtils.isBlank(e.getCause().toString()) || !StringUtils.isEmpty(e.getCause().toString())) {
            Log.error(" " + method + " -" + " " + e.getCause().toString());
        }
        Assert.fail(message);
    }

    protected WebElement getElement(LocatorType locatorType, String locator) {
        WebElement element;
        switch (locatorType) {
            case Id:
                element = driver.findElement(By.id(locator));
                break;
            case Name:
                element = driver.findElement(By.name(locator));
                break;
            case Css:
                element = driver.findElement(By.cssSelector(locator));
                break;
            case Class:
                element = driver.findElement(By.className(locator));
                break;
            case Link:
                element = driver.findElement(By.linkText(locator));
                break;
            case XPath:
                element = driver.findElement(By.xpath(locator));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + locatorType);
        }
        return element;
    }

    protected void waitElement(LocatorType locatorType, String locator) {
        switch (locatorType) {
            case Id:
                wait.until(ExpectedConditions.presenceOfElementLocated(By.id(locator)));
                break;
            case Name:
                wait.until(ExpectedConditions.presenceOfElementLocated(By.name(locator)));
                break;
            case Css:
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
                break;
            case Class:
                wait.until(ExpectedConditions.presenceOfElementLocated(By.className(locator)));
                break;
            case Link:
                wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(locator)));
                break;
            case XPath:
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + locatorType);
        }
    }

    void sendKeys(String method, LocatorType locatorType, String locator, String input) {
        try {
            if (!StringUtils.isBlank(input) || !StringUtils.isEmpty(input)) {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                element.clear();
                element.sendKeys(input);
                Log.info(" " + method + " -" + " ..Sent data to element with locator: " + locator +
                        " ..LocatorType: " + locatorType + " ..Data entered: " + input);
            } else {
                Log.info(" " + method + " -" + " ..NO data sent to element with locator: " + locator +
                        " ..LocatorType: " + locatorType + " ..Data entered: " + input);
            }
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! NO data sent to element with locator: " + locator +
                    " ..LocatorType: " + locatorType + " ..Data entered: " + input;
            errorHandler(method, errorMessage, e);
        }
    }

    void clearField(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.clear();
            Log.info(" " + method + " -" + " ..Element cleared with locator: " + locator +
                    " ..LocatorType: " + locatorType);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT cleared with locator: " + locator +
                    " ..LocatorType: " + locatorType;
            errorHandler(method, errorMessage, e);
        }
    }

    void elementClick(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.click();
            Log.info(" " + method + " -" + " ..Element clicked with locator: " + locator +
                    " ..LocatorType: " + locatorType);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT clicked with locator: " + locator +
                    " ..LocatorType: " + locatorType;
            errorHandler(method, errorMessage, e);
        }
    }

    void elementSelect(String method, LocatorType locatorType, String locator, String select) {
        try {
            if (!StringUtils.isBlank(select) || !StringUtils.isEmpty(select)) {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                new Select(element).selectByVisibleText(select);
                Log.info(" " + method + " -" + " ..Element selected with locator: " + locator +
                        " ..LocatorType: " + locatorType + " ..Selection: " + select);
            } else {
                Log.info(" " + method + " -" + " ..NO element selected with locator: " + locator +
                        " ..LocatorType: " + locatorType + " ..Selection: " + select);
            }
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! NO element selected with locator: " + locator +
                    " ..LocatorType: " + locatorType + " ..Data entered: " + select;
            errorHandler(method, errorMessage, e);
        }
    }

    void elementCheck(String method, LocatorType locatorType, String locator, String option) {
        try {
            if (!StringUtils.isBlank(option) || !StringUtils.isEmpty(option)) {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                boolean checked = element.isSelected();
                if (!checked && option.toLowerCase().equals("y")) {
                    //check
                    element.click();
                    Log.info(" " + method + " -" + " ..Element checked with locator: " + locator +
                            " ..LocatorType: " + locatorType);
                }
                if (checked && !"y".equals(option.toLowerCase())) {
                    //uncheck
                    element.click();
                    Log.info(" " + method + " -" + " ..Element unchecked with locator: " + locator +
                            " ..LocatorType: " + locatorType);
                }
            } else
                Log.info(" " + method + " -" + " ..Element NOT checked or unchecked with locator: " + locator +
                        " ..LocatorType: " + locatorType);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT checked or unchecked with locator: " + locator +
                    " ..LocatorType: " + locatorType;
            errorHandler(method, errorMessage, e);
        }
    }

    private List getElementList(LocatorType locatorType, String locator) {
        List elements;
        switch (locatorType) {
            case Id:
                elements = driver.findElements(By.id(locator));
                break;
            case Name:
                elements = driver.findElements(By.name(locator));
                break;
            case Css:
                elements = driver.findElements(By.cssSelector(locator));
                break;
            case Class:
                elements = driver.findElements(By.className(locator));
                break;
            case Link:
                elements = driver.findElements(By.linkText(locator));
                break;
            case XPath:
                elements = driver.findElements(By.xpath(locator));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + locatorType);
        }
        return elements;
    }

    int getElementCount(String method, LocatorType locatorType, String locator) {
        try {
            int listCount;
            listCount = getElementList(locatorType, locator).size();
            Log.info(" " + method + " -" + " ..Element count with locator: " + locator +
                    " ..LocatorType: " + locatorType + " ..Count: " + listCount);
            return listCount;
        } catch (Exception e) {
            Log.info(" " + method + " -" + " ..!!NOT FOUND!! Element count NOT possible with locator: " + locator +
                    " ..LocatorType: " + locatorType);
            Log.info(" " + method + " -" + " " + e.getMessage());
            return 0;
        }
    }

    String getText(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            String text = element.getText();
            if (StringUtils.isBlank(text) || StringUtils.isEmpty(text)) {
                text = element.getAttribute("value");
            }
            if (StringUtils.isBlank(text) || StringUtils.isEmpty(text)) {
                text = element.getAttribute("innerText");
            }
            if (!StringUtils.isBlank(text) || !StringUtils.isEmpty(text)) {
                text = text.trim();
            }
            Log.info(" " + method + " -" + " ..Element get text with locator: " + locator +
                    " ..LocatorType: " + locatorType + "..Text found: " + text);
            return text;
        } catch (Exception e) {
            Log.info(" " + method + " -" + " ..!!NOT FOUND!! Element get text NOT possible with locator: " + locator +
                    " ..LocatorType: " + locatorType);
            Log.info(" " + method + " -" + " " + e.getMessage());
            return null;
        }
    }

    boolean writeProtectedText(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            boolean result;
            String text = element.getAttribute("disabled");
            result = text.equalsIgnoreCase("true");
            Log.info(" " + method + " -" + " ..Element write protected with locator: " + locator +
                    " ..LocatorType: " + locatorType + "..Result: " + result);
            return result;
        } catch (Exception e) {
            Log.info(" " + method + " -" + " ..!!NOT FOUND!! Unable to see if element is write protected with locator: "
                    + locator + " ..LocatorType: " + locatorType);
            Log.info(" " + method + " -" + " " + e.getMessage());
            return false;
        }
    }

    boolean isElementPresent(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            getElement(locatorType, locator);
            Log.info(" " + method + " -" + " ..Element present with locator: " + locator +
                    " ..LocatorType: " + locatorType);
            return true;
        } catch (Exception e) {
            Log.info(" " + method + " -" + " ..Element NOT present with locator: " + locator +
                    " ..LocatorType: " + locatorType);
            Log.info(" " + method + " -" + " " + e.getMessage());
            return false;
        }
    }

    boolean isElementDisplayed(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            if (element.isDisplayed()) {
                Log.info(" " + method + " -" + " ..Element displayed with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } else {
                Log.info(" " + method + " -" + " ..Element NOT displayed with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            }
            return element.isDisplayed();
        } catch (Exception e) {
            Log.info(" " + method + " -" + " ..Element NOT displayed with locator: " + locator +
                    " ..LocatorType: " + locatorType);
            Log.info(" " + method + " -" + " " + e.getMessage());
            return false;
        }
    }

    boolean isElementChecked(String method, LocatorType locatorType, String locator) {
        try {
            WebElement element = getElement(locatorType, locator);
            if (element.isSelected()) {
                Log.info(" " + method + " -" + " ..Element checked with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } else {
                Log.info(" " + method + " -" + " ..Element NOT checked with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            }
            return element.isSelected();
        } catch (Exception e) {
            Log.info(" " + method + " -" + " ..Element NOT checked with locator: " + locator +
                    " ..LocatorType: " + locatorType);
            Log.info(" " + method + " -" + " " + e.getMessage());
            return false;
        }
    }

    boolean elementPresenceCheck(String method, LocatorType locatorType, String locator) {
        try {
            List elements = getElementList(locatorType, locator);
            if (elements.size() > 0) {
                Log.info(" " + method + " -" + " ..Element presence check with locator: " + locator +
                        " ..LocatorType: " + locatorType);
                return true;
            } else {
                Log.info(" " + method + " -" + " ..NO element presence check with locator: " + locator +
                        " ..LocatorType: " + locatorType);
                return false;
            }
        } catch (Exception e) {
            Log.info(" " + method + " -" + " ..!!ERROR!! NO element presence check with locator: " + locator +
                    " ..LocatorType: " + locatorType);
            Log.info(" " + method + " -" + " " + e.getMessage());
            return false;
        }
    }

    void webScroll(String method, String direction) {
        try {
            JavascriptExecutor je = (JavascriptExecutor) driver;
            switch (direction.toLowerCase()) {
                case "up":
                    je.executeScript("window.scrollBy(0,-1000)");
                    Log.info(" " + method + " -" + " ..Window scrolled up");
                    break;
                case "down":
                    je.executeScript("window.scrollBy(0,1000)");
                    Log.info(" " + method + " -" + " ..Window scrolled down");
                    break;
                case "right":
                    je.executeScript("window.scrollBy(1000,0)");
                    Log.info(" " + method + " -" + " ..Window scrolled right");
                    break;
                case "left":
                    je.executeScript("window.scrollBy(-1000,0)");
                    Log.info(" " + method + " -" + " ..Window scrolled left");
                    break;
            }
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Unable to scroll";
            errorHandler(method, errorMessage, e);
        }
    }

    void switchFrame(String method, String frame) {
        try {
            driver.switchTo().frame(frame);
            Log.info(" " + method + " -" + " ..Switched to frame: " + frame);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Unable to switch to frame: " + frame;
            errorHandler(method, errorMessage, e);
        }
    }

    void switchWindow(String method) {
        try {
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
                Log.info(" " + method + " -" + " ..Switched to window");
            }
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Unable to switch to window";
            errorHandler(method, errorMessage, e);
        }
    }

    void alertResponse(String method, String response) {
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            if (response.toLowerCase().equals("y")) {
                alert.accept();
                Log.info(" " + method + " -" + " ..Alert accepted");
            } else {
                alert.dismiss();
                Log.info(" " + method + " -" + " ..Alert dismissed");
            }
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Unable to handle alert";
            errorHandler(method, errorMessage, e);
        }
    }

    void dragAndDrop(String method, LocatorType fromLocatorType, String fromLocator, LocatorType toLocatorType, String toLocator) {
        try {
            waitElement(fromLocatorType, fromLocator);
            WebElement fromElement = getElement(fromLocatorType, fromLocator);
            WebElement toElement = getElement(toLocatorType, toLocator);
            Actions act = new Actions(driver);
            act.dragAndDrop(fromElement, toElement).build().perform();
            Log.info(" " + method + " -" + " ..Drag and drop From locator: " + fromLocator + "..From locatorType: " + fromLocatorType + ".. To locator: " + toLocator + ".. To locatorType: " + toLocatorType);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Unable to drag and drop";
            errorHandler(method, errorMessage, e);
        }
    }

    void mouseHovering(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            Actions act = new Actions(driver);
            act.moveToElement(element).build().perform();
            Log.info(" " + method + " -" + " ..Mouse hovering to locator: " + locator + "..LocatorType" + locatorType);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Unable to hover with mouse";
            errorHandler(method, errorMessage, e);
        }
    }

    void sleep(String method, long seconds) {
        try {
            Thread.sleep(seconds * 1000);
            Log.info(" " + method + " -" + " ..Wait :: " + seconds + " seconds");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void elementBackSpace(String method, LocatorType locatorType, String locator, String repeats) {
        int n = 1;
        while (n <= Integer.parseInt(repeats)) {
            try {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                element.sendKeys(Keys.BACK_SPACE);
                Log.info(" " + method + " -" + " ..Element back space with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } catch (Exception e) {
                String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT back spaced with locator: " + locator +
                        " ..LocatorType: " + locatorType;
                errorHandler(method, errorMessage, e);
            }
            n++;
        }
    }

    void elementUpArrow(String method, LocatorType locatorType, String locator, String repeats) {
        int n = 1;
        while (n <= Integer.parseInt(repeats)) {
            try {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                element.sendKeys(Keys.ARROW_UP);
                Log.info(" " + method + " -" + " ..Element up arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } catch (Exception e) {
                String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT up arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType;
                errorHandler(method, errorMessage, e);
            }
            n++;
        }
    }

    void elementDownArrow(String method, LocatorType locatorType, String locator, String repeats) {
        int n = 1;
        while (n <= Integer.parseInt(repeats)) {
            try {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                element.sendKeys(Keys.ARROW_DOWN);
                Log.info(" " + method + " -" + " ..Element down arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } catch (Exception e) {
                String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT down arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType;
                errorHandler(method, errorMessage, e);
            }
            n++;
        }
    }

    void elementLeftArrow(String method, LocatorType locatorType, String locator, String repeats) {
        int n = 1;
        while (n <= Integer.parseInt(repeats)) {
            try {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                element.sendKeys(Keys.ARROW_LEFT);
                Log.info(" " + method + " -" + " ..Element left arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } catch (Exception e) {
                String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT left arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType;
                errorHandler(method, errorMessage, e);
            }
            n++;
        }
    }

    void elementRightArrow(String method, LocatorType locatorType, String locator, String repeats) {
        int n = 1;
        while (n <= Integer.parseInt(repeats)) {
            try {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                element.sendKeys(Keys.ARROW_RIGHT);
                Log.info(" " + method + " -" + " ..Element right arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } catch (Exception e) {
                String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT right arrow key with locator: " + locator +
                        " ..LocatorType: " + locatorType;
                errorHandler(method, errorMessage, e);
            }
            n++;
        }
    }

    void elementDeleteAll(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.sendKeys(Keys.CONTROL, "a", Keys.DELETE);
            Log.info(" " + method + " -" + " ..Element delete all with locator: " + locator +
                    " ..LocatorType: " + locatorType);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT all deleted with locator: " + locator +
                    " ..LocatorType: " + locatorType;
            errorHandler(method, errorMessage, e);
        }
    }

    void elementEnter(String method, LocatorType locatorType, String locator) {
        try {
            waitElement(locatorType, locator);
            WebElement element = getElement(locatorType, locator);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.sendKeys(Keys.ENTER);
            Log.info(" " + method + " -" + " ..Element enter all with locator: " + locator +
                    " ..LocatorType: " + locatorType);
        } catch (Exception e) {
            String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT all entered with locator: " + locator +
                    " ..LocatorType: " + locatorType;
            errorHandler(method, errorMessage, e);
        }
    }

    void elementTabKey(String method, LocatorType locatorType, String locator, String repeats) {
        int n = 1;
        while (n <= Integer.parseInt(repeats)) {
            try {
                waitElement(locatorType, locator);
                WebElement element = getElement(locatorType, locator);
                wait.until(ExpectedConditions.elementToBeClickable(element));
                element.sendKeys(Keys.TAB);
                Log.info(" " + method + " -" + " ..Element tab key with locator: " + locator +
                        " ..LocatorType: " + locatorType);
            } catch (Exception e) {
                String errorMessage = " " + method + " -" + " ..!!ERROR!! Element NOT tab key with locator: " + locator +
                        " ..LocatorType: " + locatorType;
                errorHandler(method, errorMessage, e);
            }
            n++;
        }
    }
//# Prov2:6
}
package pages;

import org.openqa.selenium.WebDriver;

public class ItemsTakenPage extends ActionBase {

    private String page = Thread.currentThread().getStackTrace()[1].getClassName();

    public ItemsTakenPage(WebDriver driver) {
        super(driver);
    }

    public String returnIncidentNumber() {
        return getText(page, LocatorType.XPath, "//div[@id='itemTaken']//div[@class='panel-heading']");
    }

    public void buttonAddItemTaken() {
        click(page, LocatorType.XPath, "//button[@class='btn-actions pull-right']");
    }

    public void dropdownItemsTaken(String option) {
        selectItem(page, LocatorType.XPath, "//table[@class='table table-condensed table-hover']//select[@class='form-control']", option);
    }

    public void buttonAddItem() {
        click(page, LocatorType.XPath, "//a[@class='btn btn-success']");
    }

    public void buttonRemoveItem() {
        click(page, LocatorType.XPath, "//a[@class='btn btn-danger']");
    }

    public void buttonSaveChanges() {
        click(page, LocatorType.XPath, "//button[contains(text(),'Save Changes for Items Taken')]");
    }
}
package util;

import java.util.Random;

public class Functions {

    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
            Log.info("Wait :: " + millis + " milliseconds");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getAlphaNumeric(int length, String type) {
        String mix = "abcdefghijklmnopqrstuvwxyz1234567890";
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        String number = "1234567890";
        StringBuilder randStr = new StringBuilder();

        switch (type.toLowerCase()) {
            case "lower":
                for (int i = 0; i < length; i++) {
                    int randomNumber = getRandomNumber(alpha);
                    char ch = alpha.charAt(randomNumber);
                    randStr.append(ch);
                }
                return randStr.toString().toLowerCase();
            case "upper":
                for (int i = 0; i < length; i++) {
                    int randomNumber = getRandomNumber(alpha);
                    char ch = alpha.charAt(randomNumber);
                    randStr.append(ch);
                }
                return randStr.toString().toUpperCase();
            case "digits":
                for (int i = 0; i < length; i++) {
                    int randomNumber = getRandomNumber(number);
                    char ch = alpha.charAt(randomNumber);
                    randStr.append(ch);
                }
                return randStr.toString();
            case "mix":
                for (int i = 0; i < length; i++) {
                    int randomNumber = getRandomNumber(mix);
                    char ch = alpha.charAt(randomNumber);
                    randStr.append(ch);
                }
                return randStr.toString();
            default:
                return null;
        }
    }

    private int getRandomNumber(String listType) {
        int randomInt;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(listType.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }





}
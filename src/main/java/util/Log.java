package util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log {

    // Initialise log4j logs
    private static final Logger log = LogManager.getLogger(Log.class.getName());

    // Print log for the beginning of a test case
    public static void startTestCase(String testCaseName) {
        log.info(" ############ START  " + testCaseName + " START  ############");
//        log.info(" ###### START  size " + WebDriverHelper.windowSize + " size  START ######");
    }

    // Print log for the end of a test case
    public static void endTestCase(String testCaseName) {
        log.info(" ************ END    " + testCaseName + " END    ************");
    }

    // Message types
    public static void info(String message) {
        log.info(message);
    }

    public static void warn(String message) {
        log.warn(message);
    }

    public static void error(String message) {
        log.error(message);
    }

    public static void fatal(String message) {
        log.fatal(message);
    }

    public static void debug(String message) {
        log.debug(message);
    }
}
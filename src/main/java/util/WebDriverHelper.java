//package util;
//
//import org.openqa.selenium.Dimension;
//import org.openqa.selenium.PageLoadStrategy;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.edge.EdgeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.security.InvalidParameterException;
//import java.util.Properties;
//import java.util.concurrent.TimeUnit;
//
//public class WebDriverHelper {
//    static String windowSize;
//
//    public static WebDriver createDriver(String browser) throws IOException {
//
//        WebDriver driver;
//        Properties testConfig = new Properties();
//        testConfig.load(new FileInputStream("TestConfig.properties"));
//        String width = testConfig.getProperty("width");
//        String height = testConfig.getProperty("height");
//        String location = testConfig.getProperty("drivers");
//
//
//        System.setProperty("log4j.configurationFile", "log4j2.xml");
//
//        if (browser.equalsIgnoreCase("Firefox")) {
//            System.setProperty("webdriver.gecko.driver", location + "geckodriver.exe");
//            FirefoxOptions firefoxOptions = new FirefoxOptions();
//            firefoxOptions.setCapability("marionette", true);
//            driver = new FirefoxDriver(firefoxOptions);
//        } else if (browser.equalsIgnoreCase("Chrome")) {
//            System.setProperty("webdriver.chrome.driver", location + "chromedriver.exe");
//            driver = new ChromeDriver();
//        } else if (browser.equalsIgnoreCase("IE")) {
//            System.setProperty("webdriver.ie.driver", location + "IEDriverServer.exe");
//            driver = new InternetExplorerDriver();
//        } else if (browser.equalsIgnoreCase("Edge")) {
//            System.setProperty("webdriver.edge.driver", location + "MicrosoftWebDriver.exe");
//            driver = new EdgeDriver();
//        } else if (browser.equalsIgnoreCase("Headless")) {
//            System.setProperty("webdriver.chrome.driver", location + "chromedriver.exe");
//            ChromeOptions chromeOptions = new ChromeOptions();
//            chromeOptions.addArguments("enable-automation");
//            chromeOptions.addArguments("--headless");
//            chromeOptions.addArguments("--window-size=1920,1080");
//            chromeOptions.addArguments("--no-sandbox");
//            chromeOptions.addArguments("--disable-extensions");
//            chromeOptions.addArguments("--dns-prefetch-disable");
//            chromeOptions.addArguments("--disable-gpu");
//            chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
//            driver = new ChromeDriver(chromeOptions);
//        } else {
//            throw new InvalidParameterException(browser + " - is not a valid web browser for web driver.");
//        }
//
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//        if (width != null && width.length() > 0 && height != null && height.length() > 0) {
//            Dimension dim = new Dimension(Integer.parseInt(width), Integer.parseInt((height)));
//            driver.manage().window().setSize(dim);
//        } else {
//            driver.manage().window().maximize();
//        }
//
//        windowSize = driver.manage().window().getSize().toString();
//        return driver;
//    }
//
//    public static void quitDriver(WebDriver driver) {
//        driver.quit();
//    }
//}
package endpoints;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.ObjectUtils;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Bookings {


    boolean t = true;
    boolean f = false;
    String n = null;


    private String page = Thread.currentThread().getStackTrace()[1].getClassName();
    public JsonObject jsonParameter = new JsonObject();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static String jsonpath = System.getProperty("user.dir");

    public Bookings(){
        fileReader();
    }

    public void bookingdates(String checkin, String checkout){

        Map<String, String> dates = new HashMap<>();
        dates.put("checkin", checkin);
        dates.put("checkout", checkout);
        jsonParameter.add("bookingdates", gson.toJsonTree(dates));
    }


    public void firstname(String firstname){
        jsonParameter.addProperty("firstname", stringVal(firstname));
    }

    public void lastname(String lastname){
        jsonParameter.addProperty("lastname", stringVal(lastname));
    }

    public void totalPrice(String totalPrice){
        jsonParameter.addProperty("totalprice", Integer.parseInt(totalPrice));
    }

    public void depositPaid(String depositPaid){
        jsonParameter.addProperty("depositpaid", booleanVal(depositPaid));
    }

    public void additionalNeeds(String additionalNeeds){
        jsonParameter.addProperty("additionalneeds", stringVal(additionalNeeds));
    }


    public void fileReader() {
        try{
            jsonParameter = gson.fromJson(new FileReader(jsonpath+"/test.json"),JsonObject.class);
        }catch (FileNotFoundException e){
            System.out.println("file not found");
        }

    }

    public String stringVal(String param){

        if(param.equals("")){
            param = null;
        }
        return param;
    }

    public boolean booleanVal (String param){
        return new Boolean(param).booleanValue();
    }


    public JsonObject jsonParameter(){
        return this.jsonParameter;
    }


}
